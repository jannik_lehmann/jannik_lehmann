# Hi I'm Jannik :wave:

## About Me
- :mag: Web-Performance Nerd
- :lifter: Passionate about CrossFit
- :coffee: I run on coffee
- :drum: I'm not missing out on any live music gig I can go visit


## How I work
- I love collaborating and am always open for a pair programming session.
- Preferring a mix of asynchronous and scheduled synchronous chats.
- I value clear, constructive feedback and structured meetings.

